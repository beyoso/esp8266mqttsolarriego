/*
  Basic ESP8266 MQTT example

  This sketch demonstrates the capabilities of the pubsub library in combination
  with the ESP8266 board/library.

  It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off

  It will reconnect to the server if the connection is lost using a blocking
  reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
  achieve the same result without blocking the main loop.

  To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

*/
#include "DHT.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
// DHT
DHT dht;



// Update these with values suitable for your network.
uint32_t TIEMPO_DeepSleep = 10e6;
const char* ssid = "vereda-R";
const char* password = "vereda2019";
const char* mqtt_server = "192.168.1.100";
const int mqtt_port= 1883;
const char* nameNode ="NodeSolar";
const char* intopic = "controlriego";
const char* outtopic ="ActionRiego";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
bool st = true;
//Puente H
const int puenteh00 = D3;
const int puenteh01 = D4;
#define rele_on  0
#define rele_off 1
#define transistor D5

void SetStatePuente(int puente)
{
  //digitalWrite(transistor, transistor_on);
  //delay(500);
  digitalWrite(LED_BUILTIN,rele_on);
  if(puente == 0)
  {
    digitalWrite(puenteh00, rele_on);
    digitalWrite(puenteh01, rele_off);
    
    
  }
  if(puente ==1)
  {
    digitalWrite(puenteh00, rele_off);
    digitalWrite(puenteh01, rele_on);
    
    
  }
  delay(5);
  digitalWrite(puenteh00, rele_off);
  digitalWrite(puenteh01, rele_off);
  digitalWrite(LED_BUILTIN,rele_off);
  
}
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  int tope = 0;
  while (WiFi.status() != WL_CONNECTED || tope ==5) {
    delay(500);
    Serial.print(".");
    tope++;
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(nameNode)) {
      Serial.println("connected");
      // Enviamos los datos obtenidos
     client.publish(intopic, nameNode);
      // ... and resubscribe
      client.subscribe(outtopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


float getVolBat(){
  // 820k, 270K puesto que 820k es la mayor de mis resitencias
  // y he establecido un margen en para adaptarme a fuentes de 0 y 4.2V que son las mas fiables y faciles de conseguir
  //R1= 270k y r2= 820k
  Serial.println(analogRead(A0));
  float inV = analogRead(A0)*0.00479296875;
  return 1.0;
}
char* getAndSendData()
{
  delay(dht.getMinimumSamplingPeriod());

  float h = dht.getHumidity();
  float t = dht.getTemperature();
  Serial.println("Collecting temperature & voltaje data.");
  

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to read from DHT sensor!");
    h=0.0;
    t=0.0;
    //return;
  }



  String temperature = String(t);
  String humidity = String(h);

  float vol = getVolBat();
  // Just debug messages
  Serial.print( "Sending temperature and humidity : [" );
  Serial.print( temperature ); Serial.print( "," );
  Serial.print( humidity );Serial.print( "," );
  Serial.print( vol );
  Serial.print( "]   -> " );

  // Prepare a JSON payload string
  String payload = "{";
  payload += "\"temperature\":"; payload += temperature; payload += ",";
  payload += "\"humidity\":"; payload += humidity;payload += ",";
  payload += "\"voltaje\":"; payload += vol;
  payload += "}";
  
  // Send payload
  
  char attributes[200];
  
  payload.toCharArray( attributes, 200 );
  Serial.println( attributes );/*
  if (client.publish( topic, attributes ) == true) {
    Serial.println("Success sending message");
  } else {
      Serial.println("Error sending message");
  }
 */
  return attributes;

}
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(2, LOW);   // Turn the LED on (Note that LOW is the voltage level
    
    // but actually the LED is on; this is because
    // it is acive low on the ESP-01)
    
    SetStatePuente(1);
  } else {
    
    digitalWrite(2, HIGH);  // Turn the LED off by making the voltage HIGH
    SetStatePuente(0);
    
  }

  client.publish(intopic, getAndSendData());
}
void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  digitalWrite(BUILTIN_LED, rele_on);
  pinMode(puenteh00, OUTPUT);
  pinMode(puenteh01, OUTPUT);
  
  digitalWrite(puenteh00, rele_off);
  digitalWrite(puenteh01, rele_off);
  
  
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  //dht
  dht.setup(D7); // data pin 5
  //Puente H
  digitalWrite(BUILTIN_LED, rele_off);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  
  //Enviamos los datos obtenidos
  //client.publish(intopic, getAndSendData());
  //Dormimos
  //ESP.deepSleep(TIEMPO_DeepSleep, WAKE_RF_DEFAULT);
  /*
    SetStatePuente(0,!st);
    SetStatePuente(1,!st);
    delay(3000);
    SetStatePuente(0,st);
    SetStatePuente(1,st);
    delay(3000);
    */
    
}